#!/bin/sh
# Script to record a distribution record as a tag in git.
# Depends on:
#       git 2.3.7 or better.
#       Jenkins 2.69 or better.

# The tag is placed on the commit on which Jenkins did its thing.

# The only requirement for this script is to supply a platform name,
# for example: nse, nsx, nsv

# The tag prefix of dist/ is by convention, allowing NSIngot dashboards
# to see the distribution record.

# The tag is pushed to the upstream repository used by Jenkins. Keeping the
# tag in the Jenkins clone has limited usefulness.

if [ "$1" = "" ]; then
	echo "Usage: $0 platform"
	exit 1
fi
export PLATFORM=$1

echo Creating Distribution tag in git ${GIT_URL}

echo git tag -a --file=- dist/${PLATFORM}/${BUILD_TAG} ${GIT_COMMIT}
git tag -a --file=- dist/${PLATFORM}/${BUILD_TAG} ${GIT_COMMIT} <<ANNOTATION
platform: ${PLATFORM}
url: ${BUILD_URL}
job: ${JOB_NAME}
workspace: ${WORKSPACE}
instance: ${JENKINS_URL}
git: ${GIT_URL}
ANNOTATION

echo git push origin refs/tags/dist/${PLATFORM}/${BUILD_TAG}:refs/tags/dist/${PLATFORM}/${BUILD_TAG}
git push origin refs/tags/dist/${PLATFORM}/${BUILD_TAG}:refs/tags/dist/${PLATFORM}/${BUILD_TAG}
exit $?
