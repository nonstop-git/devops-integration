# The NonStop devops-integration Project #

This repository is shared for use with git, NSGit, NSIngot, Jenkins,
and other DevOps components to share information.

### What is this repository for? ###

* Shared code and scripts used for integrating DevOps components.
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Set up a job in Jenkins that monitors this repository.
* Have Jenkins build a workspace that can be shared with other jobs.
* Related: NSGit
* Database configuration: None.
* Deployment instructions: None.

### Contribution guidelines ###

* Please use Fork Flow for contributions.
+ Use proper file extensions so we know how to execute your code:
    * Shell scripts should use .sh
    * Groovy scripts should use .groovy
    * Perl scripts should use .pl
    * Please add other extensions here if you add new ones.
+ Unit test are contained in the test directory as follows:
    * Test names should be in directories named _your-script_.
    * You can modify contributions from others, subject to Pull requests.
* Please have all reviews done by the repo owner using pull requests.
* Use this project at your own risk. It is Open Source. Please see the LICENSE file.

### Who do I talk to? ###

* The repository owner is Randall S. Becker, at randall.becker@nexbridge.ca.
* Other community or team contact
