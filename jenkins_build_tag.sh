#!/bin/sh
# Script to record a Jenkins build record as a tag in git.
# Depends on:
#	git 2.3.7 or better.
#	Jenkins 2.69 or better.

# The tag is placed on the commit on which Jenkins did its thing.

# The tag prefix of jenkins/ is by convention, allowing NSIngot dashboards
# to see the build record.

# The tag is pushed to the upstream repository used by Jenkins. Keeping the
# tag in the Jenkins clone has limited usefulness.

echo Creating Jenkins tag in git ${GIT_URL}

echo git tag -a --file=- jenkins/${BUILD_TAG} ${GIT_COMMIT}
git tag -a --file=- jenkins/${BUILD_TAG} ${GIT_COMMIT} <<ANNOTATION
url: ${BUILD_URL}
job: ${JOB_NAME}
workspace: ${WORKSPACE}
instance: ${JENKINS_URL}
git: ${GIT_URL}
ANNOTATION

echo git push origin refs/tags/jenkins/${BUILD_TAG}:refs/tags/jenkins/${BUILD_TAG}
git push origin refs/tags/jenkins/${BUILD_TAG}:refs/tags/jenkins/${BUILD_TAG}
exit $?
