#!/bin/sh
# Script to record an installation record as a tag in git.
# Depends on:
#       git 2.3.7 or better.
#       Jenkins 2.69 or better.

# The tag is placed on the commit on which Jenkins did its thing.

# The only requirement for this script is to supply a target path and
# environment name.

# The tag prefix of install/ is by convention, allowing NSIngot dashboards
# to see the installation record.

# The tag is pushed to the upstream repository used by Jenkins. Keeping the
# tag in the Jenkins clone has limited usefulness.

export PROGNAME=$0
Usage() {
	echo "Usage: $PROGNAME path environment"
	exit 1
}

if [ "$1" = "" ]; then Usage; fi
export INSTALL_PATH=$1
shift

if [ "$1" = "" ]; then Usage; fi
export ENVIRONMENT=$1
shift

if [ "$1" != "" ]; then Usage; fi

echo Creating Installation tag in git ${GIT_URL}

echo git tag -a --file=- install/${ENVIRONMENT}/${BUILD_TAG} ${GIT_COMMIT}
git tag -a --file=- install/${ENVIRONMENT}/${BUILD_TAG} ${GIT_COMMIT} <<ANNOTATION
path: ${INSTALL_PATH}
environment: ${ENVIRONMENT}
url: ${BUILD_URL}
job: ${JOB_NAME}
workspace: ${WORKSPACE}
instance: ${JENKINS_URL}
git: ${GIT_URL}
ANNOTATION

echo git push origin refs/tags/install/${ENVIRONMENT}/${BUILD_TAG}:refs/tags/install/${ENVIRONMENT}/${BUILD_TAG}
git push origin refs/tags/install/${ENVIRONMENT}/${BUILD_TAG}:refs/tags/install/${ENVIRONMENT}/${BUILD_TAG}
exit $?
